<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>MOVIE ADDICT</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="style.css">

      
</head>



<nav class="navbar navbar-expand-lg navbar-fixed-top navbar-light" style="background-color:#e3f2fd;">
  <a class="navbar-brand" href="index.html">MOVIE ADDICT</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="index.html">Home <span class="sr-only"></span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="gallery.html">Gallery</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="contact.html">Contact Us</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="movies.html" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Movies
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="movies.html">Hollywood</a>
          <a class="dropdown-item" href="movies.html">Nollywood</a>
           <a class="dropdown-item" href="movies.html">Bollywood</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="movies.html">Others</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="SignIn.html">Sign In</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="about.html">About Us</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>



<p id="#films">&nbsp;</div>

<main>
	<p id="#films">&nbsp;</div>


<p style="color: white; text-align: center">

	<?php  
		

	
		$start_year = 1980; 
		$end_year = 2018; 
		$leap_year_count = 0; 

		
		for ($i=$start_year; $i <= $end_year; $i++) { 
			if ((($i % 4) === 0 && ($i % 100) !== 0 ) || ($i % 400) === 0 ){
				$leap_year_count = $leap_year_count + 1;
				echo "$i is a leap year" . "<br>";
			} else {
				echo "$i"."<br>";
			}
		} 
		echo "The count of leap years ranging from $start_year to $end_year is ". $leap_year_count;
		?>

</p>	


</main>

   <div class="bottom" style="background-color: #e3f2fd">
           <ul>
                                             
                 
                    <a href="#"><li>Advertisement</li></a>
                    <a href="#"><li>New arrivals</li></a>
                    <a href="#"><li>Read More</li></a>
                  
                        

                    </ul>

            </div>

    </div>

  
     <footer>
             <p>&copy; MOVIE ADDICT.com</p>
        </footer>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.js"></script> 
</body>

</html>